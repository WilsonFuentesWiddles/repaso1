package Controller;

import Model.Calculadora;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ControllerCalculadora", urlPatterns = {"/ControllerCalculadora"})
public class ControllerCalculadora extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerCalculadora</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerCalculadora at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //processRequest(request, response);
        
        String numero1 = request.getParameter("txtNumero1");
        String numero2 = request.getParameter("txtNumero2");
        int n1 = Integer.parseInt(numero1);
        int n2 = Integer.parseInt(numero2);
        
        Calculadora c1 = new Calculadora(n1, n2);
        request.setAttribute("calculadora", c1);
        request.getRequestDispatcher("resultado.jsp").forward(request, response);
        
        
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }

}

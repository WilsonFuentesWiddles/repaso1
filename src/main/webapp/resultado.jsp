<%-- 
    Document   : resultado
    Created on : 22-04-2021, 19:54:29
    Author     : Alex
--%>

<%@page import="Model.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Calculadora c1 = (Calculadora) request.getAttribute("calculadora");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>La suma es: <%= c1.getSuma() %></h1>
    </body>
</html>
